import 'dart:io';

import 'package:band_names/models/band.dart';
import 'package:band_names/services/socket_services.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:provider/provider.dart';

class Home extends StatefulWidget {
  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<Band> _bands = [];

  @override
  void initState() {
    final socketService = Provider.of<SocketServices>(context, listen: false);
    socketService.socket.on('active-bands', _handleActiveBands);

    super.initState();
  }

  _handleActiveBands(dynamic payload) {
    _bands = (payload as List).map((band) => Band.fromMap(band)).toList();
    setState(() {});
  }

  @override
  void dispose() {
    final socketService = Provider.of<SocketServices>(context, listen: false);
    socketService.socket.off('active-bands');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final socketService = Provider.of<SocketServices>(context);
    return Scaffold(
        appBar: AppBar(
          title:
              const Text('BandNames', style: TextStyle(color: Colors.black87)),
          backgroundColor: Colors.white,
          elevation: 1,
          actions: [
            Container(
              margin: const EdgeInsets.only(right: 10),
              child: (socketService.serverStatus == ServerStatus.online)
                  ? const Icon(Icons.check_box, color: Colors.blue)
                  : const Icon(Icons.offline_bolt, color: Colors.red),
            )
          ],
        ),
        body: Column(
          children: [
            if (Platform.isAndroid)
              const LinearProgressIndicator(
                backgroundColor: Colors.white,
                valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
              ),
            _bands.isNotEmpty ? _showGraph() : const SizedBox(),
            Expanded(
              child: ListView.builder(
                itemCount: _bands.length,
                itemBuilder: (context, i) => bandTitle(_bands[i]),
              ),
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          elevation: 1,
          onPressed: addNewBand,
          child: const Icon(Icons.add),
        ));
  }

  Widget bandTitle(Band band) {
    return Dismissible(
      key: Key(band.id),
      direction: DismissDirection.startToEnd,
      background: Container(
        padding: const EdgeInsets.only(left: 8.0),
        color: Colors.red[300],
        child: const Align(
          alignment: Alignment.centerLeft,
          child: Text(
            'Delete Band',
            style: TextStyle(color: Colors.white),
          ),
        ),
      ),
      onDismissed: (direction) {
        deleteBand(band.id);
      },
      child: ListTile(
        leading: CircleAvatar(
          backgroundColor: Colors.blue[100],
          child: Text(band.name.substring(0, 2)),
        ),
        title: Text(band.name),
        trailing: Text(
          '${band.votes}',
          style: const TextStyle(fontSize: 20),
        ),
        onTap: () {
          addVote(band.id);
        },
      ),
    );
  }

  void addVote(String id) {
    final socketService = Provider.of<SocketServices>(context, listen: false);
    socketService.socket.emit('vote-band', {'id': id});
  }

  addNewBand() {
    final textController = TextEditingController();

    if (Platform.isAndroid) {
      return showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: const Text('New Band Name'),
              content: TextField(
                controller: textController,
                onChanged: (value) {},
              ),
              actions: [
                MaterialButton(
                    elevation: 5,
                    textColor: Colors.blue,
                    onPressed: () => addBandNameToList(textController.text),
                    child: const Text('Add'))
              ],
            );
          });
    }

    showCupertinoDialog(
        context: context,
        builder: (context) {
          return CupertinoAlertDialog(
            title: const Text('New Band Name'),
            content: CupertinoTextField(
              controller: textController,
              onChanged: (value) {},
            ),
            actions: [
              CupertinoDialogAction(
                  isDefaultAction: true,
                  child: const Text('Add'),
                  onPressed: () => addBandNameToList(textController.text)),
              CupertinoDialogAction(
                isDefaultAction: true,
                onPressed: () {
                  Navigator.pop(context);
                },
                textStyle: const TextStyle(color: Colors.red),
                child: const Text('Close'),
              ),
            ],
          );
        });
  }

  void addBandNameToList(String name) {
    final socketService = Provider.of<SocketServices>(context, listen: false);
    if (name.length > 1) {
      // Podemos agregar
      socketService.socket.emit('add-band', {'name': name, 'votes': 0});
    }
    Navigator.pop(context);
  }

  void deleteBand(String id) {
    final socketService = Provider.of<SocketServices>(context, listen: false);
    socketService.socket.emit('delete-band', {'id': id});
  }

  Widget _showGraph() {
    Map<String, double> dataMap = new Map();

    for (var band in _bands) {
      setState(() {
        dataMap.putIfAbsent(band.name, () => band.votes.toDouble());
      });
    }

    return Container(
      height: 300,
      width: double.infinity,
      child: PieChart(
        dataMap: dataMap,
        animationDuration: const Duration(milliseconds: 800),
        chartLegendSpacing: 32,
        chartRadius: MediaQuery.of(context).size.width / 3.2,
        colorList: const [
          Colors.blue,
          Colors.green,
          Colors.red,
          Colors.yellow,
          Colors.orange,
          Colors.purple,
        ],
        initialAngleInDegree: 0,
        ringStrokeWidth: 32,
        centerText: "HYBRID",
        legendOptions: const LegendOptions(
          legendTextStyle: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
