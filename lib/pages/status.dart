import 'package:band_names/services/socket_services.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Status extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final socketService = Provider.of<SocketServices>(context);

    // socketService.socket.on('message', (payload) {
    //   print('nuevo-mensaje: $payload');
    // });

    return Scaffold(
      appBar: AppBar(
        title: const Text('Status', style: TextStyle(color: Colors.black87)),
        backgroundColor: Colors.white,
        elevation: 1,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('server Status: ${socketService.serverStatus}'),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.message),
        onPressed: () {
          socketService.socket.emit('message', {
            'name': 'Flutter',
            'message': 'Hola desde Flutter',
          });
        },
      ),
    );
  }
}
