import 'package:flutter/material.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;

enum ServerStatus { online, offline, connecting }

class SocketServices with ChangeNotifier {
  // Socket IO
  ServerStatus _serverStatus = ServerStatus.connecting;

  ServerStatus get serverStatus => _serverStatus;

  IO.Socket? _socket;
  IO.Socket get socket => _socket!;

  SocketServices() {
    _initConfig();
  }

  void _initConfig() {
    _socket = IO.io('http://localhost:3000', {
      'transports': ['websocket'],
      'autoConnect': true,
    });
    _socket?.onConnect((_) {
      print('connect');
      _serverStatus = ServerStatus.online;
      _socket?.emit('msg', 'test');
      notifyListeners();
    });

    _socket?.onDisconnect((_) {
      _serverStatus = ServerStatus.offline;
      print('disconnect');
      notifyListeners();
    });
  }
}
